#!/usr/bin/env python3

import socket
import sys
import re
import os
import signal

import requests

requests_table={
    "LENGTH":requests.length,
    "READ (.*)":requests.read,
    "KEYS":requests.keys,
    "SELECT (.*)":requests.select,
    "RESET":requests.reset,
}

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
s.bind(('',9999))
s.listen(5)
signal.signal(signal.SIGCHLD,signal.SIG_IGN)
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)
d={}
list=[]
zoz=[]
spl=[]

while True:

    cs,addr=s.accept()
    if os.fork()==0:
        s.close()
        f=cs.makefile("rw")
        while True:
            version_request=f.readline()
            version_request=version_request.strip()
            file=open("zoznam.txt","r")
            pattern=r'([^= ]*)=("[^"]*")'
            for line in file:
                d={}
                for m in re.finditer(pattern,line):
                    d[m.group(1)]=m.group(2)
                list.append(d)
            if re.match('VERSION \d+',version_request):
                f.write('100 OK\n')
                f.write('1.0\n')
                f.flush()
                break
            else:
                f.write('203 Need Version\n')
                f.flush()
                f.close()
                s.close()
                sys.exit(1)
                break
        while True:
            request=f.readline()
            #file=open("zoznam.txt","r")
            #pattern=r'([^= ]*)=("[^"]*")'
            if "SELECT" in request:
                spl=request.rstrip()
                spl=spl.split(" ")
                if len(spl)>1:
                    spl=spl[1].split(",")
            #sys.stdout.write(spl[0])
            if not f:
                s.close()
                sys.exit(1)
                break
            ##toto je ak klient zavrie spojenie, socket
            #os.kill(int(0), signal.SIGTERM)
            request=request.rstrip()
            for regex in requests_table:
                m=re.match(regex,request)
                if m:
                    if "SELECT" or "RESET" in request:
                        z=requests_table[regex](m,f,list,spl)
                        spl=z
                    else:
                        requests_table[regex](m,f,list,spl)
                    break
            else:
                f.write('202 Unknown command\n')
                #s.close()
                #f.close()
                #sys.exit(1)
            f.flush()
